const fs = require('fs');
const glob = require('glob');

const VERBOSE = !![...process.argv].find(a => a == '-v');

function getUnsortedLinesFromLogs() {
	return new Promise(async (res, rej) => {
		glob('logs/*.log', async (err, files) => {
			if(err) {
				return rej(err);
			}

			const lines = await Promise.all(files.map(file => {
				return new Promise((res, rej) => {
					fs.readFile(file, (err, data) => {
						if(err) {
							return rej(err);
						}

						res(data.toString().split(/[\n\r]+/));
					});
				});
			}))

			let flatLines = [];

			lines.forEach(lineChunk => flatLines = flatLines.concat(lineChunk));

			res(flatLines);
		});
	});
}

function outputJson(file, obj) {
	return new Promise((res, rej) => {
		fs.writeFile(file, JSON.stringify(obj, null, 2), (err, data) => {
			if(err) {
				return rej(err);
			}

			console.log('output ' + file);
			res();
		});
	});
}

async function aggregateLog(log) {
	const timestampByIp = log
		.filter(line => line.match('Share accepted at diff'))
		.map(line => {
			return {
				ip: line.match(/\[([0-9.]+)\]\s*$/)[1],
				time: + new Date(line.match(/\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d{3})\]/)[1])
			};
		})
		.sort((a, b) => {
			return a.time < b.time ? -1 : a.time > b.time ? 1 : 0;
		})
		.reduce((all, share) => {
			all[share.ip] = all[share.ip] || [];
			all[share.ip].push(share.time);

			return all;
		}, {})

	const shareActivity = Object.keys(timestampByIp)
		.map(ip => ({ ip, times: timestampByIp[ip] }))
		.map(ip => {
			let timesActive = [];

			let shareCountInPeriod = 0;
			let currentSretchStartIndex = 0;
			for(let i = 1, len = ip.times.length; i < len; i++) {
				shareCountInPeriod++;

				if(ip.times[i] - ip.times[i - 1] > 3600000 || i == len - 1) { // 1 hour or last timestamp
					timesActive.push({
						start: ip.times[currentSretchStartIndex],
						end: ip.times[i],
						elapsed: ip.times[i] - ip.times[currentSretchStartIndex],
						shares: shareCountInPeriod
					});
					shareCountInPeriod = 0;
					currentSretchStartIndex = i + 1;
				}
			}

			return { ip: ip.ip, timesActive };
		});

	return shareActivity;
}

function getWorkingHoursByIp(timesActive) {
	const tsAsHours = t => t / 1000 / 60 / 60;

	let workingHoursByIp = timesActive
		.reduce((all, ip) => {
			all[ip.ip] = all[ip.ip] || 0;

			ip.timesActive.forEach(spurt => {
				all[ip.ip] += tsAsHours(spurt.end - spurt.start);
			});

			return all;
		}, {});

	Object.keys(workingHoursByIp).forEach(key => workingHoursByIp[key] = workingHoursByIp[key].toFixed(2));
}

async function main() {
	const unsortedLogLines = await getUnsortedLinesFromLogs();

	console.time('took');

	const timesActive = await aggregateLog(unsortedLogLines);
	//console.unsortedLogLines(JSON.stringify(timesActive, null, 2));

	console.timeEnd('took');

	if(VERBOSE) {
		await outputJson('out.json', timesActive);
	} else {
		await outputJson('out.json', getWorkingHoursByIp(timesActive));
	}
}

main();
